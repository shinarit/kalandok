import Collectionz from "./collections";

const GenericBemesélés = {
  name: "Bemesélés",
  questions: [
    {
      name: "Ki a megbízó?",
      options: [Collectionz.Szereplő,],
    },
    {
      name: "Mi az a hely, ahol a csapat a megbízást kapja?",
      options: [Collectionz.Helyszín,],
    },
    {
      name: "Mi lesz a csapat motivációja?",
      options: [Collectionz["Csapat motiváció"],],
    },
  ],
}

const Körülmény = {
  name: "Körülmény",
  questions: [
    {
      name: "Milyen körülmények közt zajlik a történet?",
      options: [Collectionz.Körülmény,],
    },
    {
      name: "Miképp kapcsolódik a körülmény a történethez?",
      options: [Collectionz["Körülmény kapcsolat"],],
    },
  ],
}

const Szereplő = {
  name: "Szereplő",
  questions: [
    {
      name: "Ki a szereplő?",
      options: [Collectionz.Szereplő,],
    },
    {
      name: "Mi a szereplő motivációja?",
      options: [Collectionz["Szereplő/Bestia motiváció"],],
    },
    {
      name: "Milyen ez a szereplő?",
      options: [Collectionz.Jellemző,],
    },
  ],
}

const Bestia = {
  name: "Bestia",
  questions: [
    {
      name: "Milyen bestia?",
      options: [Collectionz.Bestia,],
    },
    {
      name: "Mi a bestia szerepe?",
      options: [Collectionz["Szereplő/Bestia motiváció"],],
    },
    {
      name: "Milyen ez a bestia?",
      options: [Collectionz.Jellemző,],
    },
  ],
}

const Helyszín = {
  name: "Helyszín",
  questions: [
    {
      name: "Mi ez a helyszín?",
      options: [Collectionz.Helyszín,],
    },
    {
      name: "Mi a helyszín szerepe a történetben?",
      options: [Collectionz["Helyszín szerepe"],],
    },
  ],
}

const Fordulat = {
  name: "Fordulat",
  questions: [
    {
      name: "Milyen fordulat jelentkezik csavarként a történetben?",
      options: [Collectionz.Fordulat,],
    },
  ],
}

const GenericNyom = {
  name: "Nyom",
  questions: [
    {
      name: "Mi ez a célhoz vezető nyom?",
      options: [Collectionz.Nyom,],
    },
    {
      name: "Miképp lehet szert tenni erre a nyomra?",
      options: [Collectionz["Nyom megszerzés"],],
    },
  ],
}

const SpookyNyom = {
  name: "Nyom",
  questions: [
    {
      name: "Mi ez a célhoz vezető nyom?",
      options: [Collectionz.Nyom, Collectionz.Jelenség,],
    },
    {
      name: "Miképp lehet szert tenni erre a nyomra?",
      options: [Collectionz["Nyom megszerzés"],],
    },
  ],
}

export const Missionz = {
  "Bajkeverők: számolj le az ellenlábasokkal": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Kik az, akivel le kell számolni?",
            options: [Collectionz.Frakció,],
          },
          {
            name: "Hol történik a főcselekmény?",
            options: [Collectionz.Helyszín,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 2,],
      },
      {
        ...Szereplő,
        recommended: [1, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [0, 1,],
      },
    ],
  },
  "Kincsvadászat: találd meg a kincset": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Pontosan mi is ez a kincs?",
            options: [Collectionz.Tárgy,],
          },
          {
            name: "Kik az, aki valójában birtokolja ezt a kincset?",
            options: [Collectionz.Frakció, Collectionz.Szereplő,],
          },
          {
            name: "Mi az a hely, ahol ez a kincs van?",
            options: [Collectionz.Helyszín,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 2,],
      },
      {
        ...Szereplő,
        recommended: [2, 3,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...GenericNyom,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [1, 3,],
      },
      {
        ...Fordulat,
        recommended: [0, 1,],
      },
    ],
  },
  "Furcsaság: derítsd ki a különleges történések hátterét": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Pontosan mi is a furcsaság?",
            options: [Collectionz.Jelenség,],
          },
          {
            name: "Ki felel valójában a furcsaságért?",
            options: [Collectionz.Frakció, Collectionz.Szereplő, Collectionz.Ősi,],
          },
          {
            name: "Mi az a hely, ahol felütötte fejét ez a furcsaság?",
            options: [Collectionz.Helyszín, Collectionz.Rom,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 2,],
      },
      {
        ...Szereplő,
        recommended: [2, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...GenericNyom,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 2,],
      },
      {
        ...Fordulat,
        recommended: [0, 1,],
      },
    ],
  },
  "Verseny: győzz le mindenki mást egy kihívásban": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Pontosan mi is a cél?",
            options: [Collectionz.Verseny,],
          },
          {
            name: "Ki a rivális fél?",
            options: [Collectionz.Frakció, Collectionz.Szereplő,],
          },
          {
            name: "Mi az a hely, ahol a versengés zajlik?",
            options: [Collectionz.Helyszín,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 2,],
      },
      {
        ...Szereplő,
        recommended: [2, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...GenericNyom,
        recommended: [0, 2,],
      },
      {
        ...Helyszín,
        recommended: [0, 2,],
      },
      {
        ...Fordulat,
        recommended: [0, 1,],
      },
    ],
  },
  "Embervadászat: találj meg valakit": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Ki az, akit meg kell találni?",
            options: [Collectionz.Szereplő,],
          },
          {
            name: "Mi az a hely, ahol van az, akit meg kell találni?",
            options: [Collectionz.Helyszín,],
          },
          {
            name: "Ki vagy kik a fő ellenlábasok?",
            options: [Collectionz.Frakció, Collectionz.Szereplő,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 1,],
      },
      {
        ...Szereplő,
        recommended: [3, 5,],
      },
      {
        ...Bestia,
        recommended: [0, 1,],
      },
      {
        ...GenericNyom,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [1, 2,],
      },
    ],
  },
  "Elveszve: hol vagy és hogy kerülsz haza": {
    mandatory: [
      {
        name: "Bemesélés",
        questions: [
          {
            name: "Mi az a hely, ahonnét a csapat indult?",
            options: [Collectionz.Helyszín,],
          }
        ],
      },
      {
        name: "Cél",
        questions: [
          {
            name: "Mi az a hely, ahol a csapat valójában van?",
            options: [Collectionz.Helyszín, Collectionz.Rom,],
          },
          {
            name: "Ki vagy kik felelnek a csapat elveszejtéséért?",
            options: [Collectionz.Szereplő, Collectionz.Frakció, Collectionz.Bestia,],
          },
          {
            name: "Mi a felelős a csapat elveszejtéséért?",
            options: [Collectionz.Jelenség, Collectionz.Ősi,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 2,],
      },
      {
        ...Szereplő,
        recommended: [0, 2,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...GenericNyom,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [1, 2,],
      },
    ],
  },
  "Behatolás: juss be valahová egy cél érdekében": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Mi az a hely, ahová be kell jutni?",
            options: [Collectionz.Helyszín,],
          },
          {
            name: "Ki az akiknek ellenére van, hogy bejussanak oda?",
            options: [Collectionz.Szereplő, Collectionz.Frakció,],
          },
          {
            name: "Ki vagy mi az, amit őriznek ott?",
            options: [Collectionz.Szereplő, Collectionz.Tárgy, Collectionz.Bestia,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 2,],
      },
      {
        ...Szereplő,
        recommended: [2, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...GenericNyom,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 2,],
      },
      {
        ...Fordulat,
        recommended: [1, 2,],
      },
    ],
  },
  "Ősi romok: járj utána a hely eredetének": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Valójában mi is ez az ősi rom?",
            options: [Collectionz.Rom,],
          },
          {
            name: "Valójában mi az az ősi, amihez a rom köthető?",
            options: [Collectionz.Ősi,],
          },
          {
            name: "Ki vagy ki azok, akiknek van ellenére, hogy fény derüljön a titkokra?",
            options: [Collectionz.Szereplő, Collectionz.Frakció, Collectionz.Ősi,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 1,],
      },
      {
        ...Szereplő,
        recommended: [2, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...SpookyNyom,
        recommended: [2, 4,],
      },
      {
        ...Helyszín,
        recommended: [0, 2,],
      },
      {
        ...Fordulat,
        recommended: [0, 2,],
      },
    ],
  },
  "Kipurgálás: szüntesd meg valami kéretlen jelenlétét": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Pontosan mi is a jelenlét?",
            options: [Collectionz.Jelenség,],
          },
          {
            name: "Ki felel valójában a jelenlétért?",
            options: [Collectionz.Szereplő, Collectionz.Frakció, Collectionz.Ősi,],
          },
          {
            name: "Mi az a hely, ahol felütötte fejét ez a dolog?",
            options: [Collectionz.Helyszín, Collectionz.Rom,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 2,],
      },
      {
        ...Szereplő,
        recommended: [2, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 3,],
      },
      {
        ...GenericNyom,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [0, 2,],
      },
    ],
  },
  "Felfedezés: ismerj meg egy ismeretlen helyet": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Valójában mi is ez az hely?",
            options: [Collectionz.Helyszín, Collectionz.Rom,],
          },
          {
            name: "Valójában ki vagy mi a felelős e hely létrejöttéért?",
            options: [Collectionz.Szereplő, Collectionz.Jelenség, Collectionz.Frakció,],
          },
          {
            name: "Ki vagy kik azok, akiknek ellenére van, hogy fény derüljön a titokra?",
            options: [Collectionz.Frakció, Collectionz.Ősi,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 1,],
      },
      {
        ...Szereplő,
        recommended: [2, 3,],
      },
      {
        ...Bestia,
        recommended: [0, 3,],
      },
      {
        ...SpookyNyom,
        recommended: [2, 4,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [0, 2,],
      },
    ],
  },
  "Kíséret: juttas el valakit vagy valamit valahova": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Ki vagy mi az, amit el kell kísérni?",
            options: [Collectionz.Szereplő, Collectionz.Tárgy, Collectionz.Bestia,],
          },
          {
            name: "Mi az a hely, ahová el kell kísérni?",
            options: [Collectionz.Helyszín,],
          },
          {
            name: "Ki vagy kik a siker fő ellenlábasai?",
            options: [Collectionz.Frakció, Collectionz.Szereplő,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 3,],
      },
      {
        ...Szereplő,
        recommended: [3, 5,],
      },
      {
        ...Bestia,
        recommended: [0, 1,],
      },
      {
        ...Helyszín,
        recommended: [0, 4,],
      },
      {
        ...Fordulat,
        recommended: [0, 2,],
      },
    ],
  },
  "Üldözve: valaki benyújtja a számlát": {
    mandatory: [
      {
        name: "Bemesélés",
        questions: [
          {
            name: "Mi az a hely, ahol színre lépnek az üldözők?",
            options: [Collectionz.Helyszín,],
          }
        ],
      },
      {
        name: "Cél",
        questions: [
          {
            name: "Ki vagy kik akarják a csapat vesztét?",
            options: [Collectionz.Szereplő, Collectionz.Frakció,],
          },
          {
            name: "Mi az üldözők motivációja?",
            options: [Collectionz["Csapat motiváció"],],
          },
          {
            name: "Mi az a hely, ahol végetérhetnek a csapat megpróbáltatásai?",
            options: [Collectionz.Helyszín, Collectionz.Jelenség,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [1, 2,],
      },
      {
        ...Szereplő,
        recommended: [2, 4,],
      },
      {
        ...Bestia,
        recommended: [0, 1,],
      },
      {
        ...Helyszín,
        recommended: [2, 4,],
      },
      {
        ...Fordulat,
        recommended: [1, 2,],
      },
    ],
  },
  "Titok: deríts ki valamiről vagy valakiről valamit": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Ki vagy mi kapcsán kell nyomozni?",
            options: [Collectionz.Szereplő, Collectionz.Helyszín, Collectionz.Jelenség, Collectionz.Tárgy,],
          },
          {
            name: "Mit kell róla megtudni?",
            options: [Collectionz.Titok,],
          },
          {
            name: "Ki vagy kik a siker fő ellenlábasai?",
            options: [Collectionz.Szereplő, Collectionz.Frakció, Collectionz.Ősi,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 1,],
      },
      {
        ...Szereplő,
        recommended: [3, 5,],
      },
      {
        ...Bestia,
        recommended: [0, 2,],
      },
      {
        ...GenericNyom,
        recommended: [0, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [1, 2,],
      },
    ],
  },
  "Vadászat: a préda a vadonban van": {
    mandatory: [
      GenericBemesélés,
      {
        name: "Cél",
        questions: [
          {
            name: "Mi is a préda?",
            options: [Collectionz.Bestia,],
          },
          {
            name: "Milyen ez a préda?",
            options: [Collectionz.Jellemző,],
          },
        ],
      },
    ],
    optional: [
      {
        ...Körülmény,
        recommended: [0, 2,],
      },
      {
        ...Szereplő,
        recommended: [1, 4,],
      },
      {
        ...Bestia,
        recommended: [1, 3,],
      },
      {
        ...Helyszín,
        recommended: [0, 3,],
      },
      {
        ...Fordulat,
        recommended: [0, 1,],
      },
    ],
  },
}

export default Missionz;